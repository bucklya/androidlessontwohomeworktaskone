package com.example.lessontwohomeworktaskone;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayDeque;

public class MainActivity extends AppCompatActivity {

    EditText field;
    Button copy;
    Button paste;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        field = findViewById(R.id.field);
        copy = findViewById(R.id.copy);
        paste = findViewById(R.id.paste);

        final ArrayDeque<String> inputList = new ArrayDeque<String>();

        copy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!field.getText().toString().isEmpty()){
                    inputList.addLast(field.getText().toString());
                    field.setText("");
                } else {
                    Toast.makeText(v.getContext(), "Поле ввода пустое", Toast.LENGTH_SHORT).show();
                }
            }
        });
        paste.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                field.setText(inputList.pollLast());
            }
        });
    }
}
